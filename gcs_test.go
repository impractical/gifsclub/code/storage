package storage

import (
	"context"
	"os"

	"google.golang.org/api/option"

	"cloud.google.com/go/storage"
)

func init() {
	f := gcsFactory{
		serviceAccountFile: os.Getenv("GOOGLE_CREDENTIALS"),
		storeBucket:        os.Getenv("GOOGLE_STORE_BUCKET"),
		prefix:             os.Getenv("GOOGLE_PREFIX"),
	}
	if f.serviceAccountFile == "" || f.storeBucket == "" {
		return
	}
	storerFactories = append(storerFactories, f)
}

type gcsFactory struct {
	serviceAccountFile string
	storeBucket        string
	prefix             string
}

func (g gcsFactory) NewStorer(ctx context.Context) (Storer, error) {
	client, err := storage.NewClient(ctx, option.WithServiceAccountFile(g.serviceAccountFile), option.WithScopes(GCSScopes...))
	if err != nil {
		return nil, err
	}
	return NewGCS(client, g.storeBucket, g.prefix), nil
}
