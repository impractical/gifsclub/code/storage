package storage

import (
	"bytes"
	"context"
	"io"

	memdb "github.com/hashicorp/go-memdb"
)

var (
	schema = &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"file": &memdb.TableSchema{
				Name: "file",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:    "id",
						Unique:  true,
						Indexer: &memdb.StringFieldIndex{Field: "ID", Lowercase: true},
					},
				},
			},
		},
	}
)

type Memfile struct {
	ID       string
	Contents []byte
	buf      *bytes.Buffer
}

func NewMemfile(id string, in []byte) *Memfile {
	b := bytes.NewBuffer(in)
	return &Memfile{
		ID:       id,
		Contents: in,
		buf:      b,
	}
}

func (m *Memfile) Write(p []byte) (int, error) {
	n, err := m.buf.Write(p)
	if err != nil {
		return n, err
	}
	m.Contents = append(m.Contents, p...)
	return n, err
}

func (m *Memfile) Read(p []byte) (int, error) {
	return m.buf.Read(p)
}

func (m *Memfile) Close() error {
	m.buf = bytes.NewBuffer(m.Contents)
	return nil
}

type Memstore struct {
	db *memdb.MemDB
}

func (m *Memstore) Upload(ctx context.Context, hash Hashes) (io.WriteCloser, error) {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("file", "id", hash.SHA256)
	if err != nil {
		return nil, err
	}
	if exists != nil {
		return nil, nil
	}
	f := NewMemfile(hash.SHA256, []byte{})
	err = txn.Insert("file", f)
	if err != nil {
		return nil, err
	}
	txn.Commit()
	return f, nil
}

func (m *Memstore) Download(ctx context.Context, hash string) (io.ReadCloser, error) {
	txn := m.db.Txn(false)
	res, err := txn.First("file", "id", hash)
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, ErrHashNotFound
	}
	return res.(*Memfile), nil
}

func (m *Memstore) Delete(ctx context.Context, hash string) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("file", "id", hash)
	if err != nil {
		return err
	}
	if exists == nil {
		return nil
	}
	err = txn.Delete("file", exists)
	if err != nil {
		return err
	}
	txn.Commit()
	return nil
}

func NewMemstore() (*Memstore, error) {
	db, err := memdb.NewMemDB(schema)
	if err != nil {
		return nil, err
	}
	return &Memstore{
		db: db,
	}, nil
}
