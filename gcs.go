package storage

import (
	"context"
	"io"
	"path"
	"strings"

	"cloud.google.com/go/storage"
	yall "yall.in"
)

var GCSScopes = []string{"https://www.googleapis.com/auth/devstorage.read_write"}

type GCS struct {
	storeBucket *storage.BucketHandle
	prefix      string
}

func (g GCS) Upload(ctx context.Context, hash Hashes) (io.WriteCloser, error) {
	path := path.Join(g.prefix, hash.SHA256)
	logger := yall.FromContext(ctx).WithField("path", path)
	obj := g.storeBucket.Object(path)
	obj = obj.If(storage.Conditions{DoesNotExist: true})
	w := obj.NewWriter(ctx)
	w.SendCRC32C = true
	w.ObjectAttrs.CRC32C = hash.CRC32C
	logger.Debug("returning configured writer")
	return w, nil
}

func (g GCS) Download(ctx context.Context, hash string) (io.ReadCloser, error) {
	rc, err := g.storeBucket.Object(strings.Join([]string{g.prefix, hash}, "/")).NewReader(ctx)
	if err == storage.ErrObjectNotExist {
		err = ErrHashNotFound
	}
	return rc, err
}

func (g GCS) Delete(ctx context.Context, hash string) error {
	err := g.storeBucket.Object(strings.Join([]string{g.prefix, hash}, "/")).Delete(ctx)
	if err == storage.ErrObjectNotExist {
		err = nil
	}
	return err
}

func NewGCS(client *storage.Client, storeBucket, prefix string) GCS {
	return GCS{
		storeBucket: client.Bucket(storeBucket),
		prefix:      prefix,
	}
}
