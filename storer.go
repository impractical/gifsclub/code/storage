package storage

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"

	"gitlab.com/paddycarver/magic-number-checker/checker"
	yall "yall.in"
)

var (
	ErrHashNotFound = errors.New("hash not found")
)

type Dependencies struct {
	Storer Storer
}

type Hashes struct {
	CRC32C uint32
	SHA256 string
}

type File struct {
	SHA256      string
	Size        int64
	ContentType string
}

type Storer interface {
	Upload(ctx context.Context, h Hashes) (io.WriteCloser, error)
	Download(ctx context.Context, sha256 string) (io.ReadCloser, error)
	Delete(ctx context.Context, sha256 string) error
}

func Upload(ctx context.Context, d Dependencies, hash Hashes, data io.Reader) (File, error) {
	logger := yall.FromContext(ctx).WithField("storer", fmt.Sprintf("%T", d.Storer))
	logger = logger.WithField("data_source", fmt.Sprintf("%T", data))
	logger = logger.WithField("sha256_claim", hash.SHA256)
	logger = logger.WithField("crc32c_claim", hash.CRC32C)

	logger.Info("uploading")

	if rc, ok := data.(io.ReadCloser); ok {
		defer rc.Close()
	}

	fileTypeWriter := &checker.MagicNumberChecker{
		SupportedMIMEs: []string{"image/gif", "image/webp"},
	}
	hashWriter := sha256.New()
	storerWriter, err := d.Storer.Upload(ctx, hash)
	if err != nil {
		logger.WithError(err).Error("error starting upload to storer")
		return File{}, err
	}

	if storerWriter == nil {
		return File{}, nil
	}

	multi := io.MultiWriter(fileTypeWriter, hashWriter, storerWriter)

	logger.Debug("starting upload")
	size, err := io.Copy(multi, data)
	if err != nil {
		logger.WithError(err).Error("error during upload")
		return File{}, err
	}
	err = fileTypeWriter.Close()
	if err != nil {
		logger.WithError(err).Error("error closing file type checker")
		return File{}, err
	}
	err = storerWriter.Close()
	if err != nil {
		logger.WithError(err).Error("error closing storer")
		return File{}, err
	}

	logger.WithField("size", size).Debug("upload written")
	sum := hex.EncodeToString(hashWriter.Sum(nil))
	if sum != hash.SHA256 {
		logger = logger.WithField("real_hash", string(sum))
		logger.Debug("file did not match hash, deleting")
		err = d.Storer.Delete(ctx, hash.SHA256)
		if err != nil {
			logger.WithError(err).Error("error deleting file that did not match hash")
			return File{}, err
		}
		logger.Debug("deleted file that did not match hash")
		return File{}, errors.New("supplied hash did not match file hash")
	}
	f := File{
		SHA256:      sum,
		Size:        size,
		ContentType: fileTypeWriter.MatchedMIME,
	}
	logger.Info("completed upload")
	return f, nil
}

func Download(ctx context.Context, d Dependencies, dst io.Writer, hash string) error {
	logger := yall.FromContext(ctx).WithField("storer", fmt.Sprintf("%T", d.Storer))
	logger = logger.WithField("data_destination", fmt.Sprintf("%T", dst))
	logger = logger.WithField("hash", hash)

	logger.Info("downloading")

	if wc, ok := dst.(io.WriteCloser); ok {
		defer wc.Close()
	}
	rc, err := d.Storer.Download(ctx, hash)
	if err != nil {
		logger.WithError(err).Error("error starting download")
		return err
	}
	defer rc.Close()
	logger.Info("starting to send data to destination")
	_, err = io.Copy(dst, rc)
	if err != nil {
		return err
	}
	logger.Info("completed download")
	return nil
}
